package knapsack

import (
	"math/bits"
	"sort"

	"gitlab.com/starius/knapsack/uint128"
)

type Logger func(message string, args ...interface{})

func Knapsack(items []int64, maxSum, maxMemoryBytes int64, logger Logger) []int64 {
	sort.Slice(items, func(i, j int) bool {
		return items[i] < items[j]
	})

	// Exclude too large items.
	filtered := make([]int64, 0, len(items))
	for _, item := range items {
		if item <= maxSum {
			filtered = append(filtered, item)
		}
	}

	items = filtered

	// Find step to fit into maxMemoryBytes.
	bytesPerCell := 0
	if len(items) <= 32 {
		bytesPerCell = 4
	} else if len(items) <= 64 {
		bytesPerCell = 8
	} else if len(items) <= 128 {
		bytesPerCell = 16
	} else {
		bytesPerCell = (len(items) + 31) / 32 * 8
	}

	var step int64
	for step = 1; ; step++ {
		if int64(bytesPerCell)*(maxSum/step) < maxMemoryBytes {
			break
		}
	}
	logger("Using step of %d real units in knapsack table.\n", step)

	items2 := make([]int64, len(items))
	for i, item := range items {
		items2[i] = (item + int64(step) - 1) / int64(step)
	}
	maxSum2 := maxSum / step

	var result2 []int
	if len(items2) <= 32 {
		result2 = knapsack_32(items2, maxSum2, logger)
	} else if len(items2) <= 64 {
		result2 = knapsack_64(items2, maxSum2, logger)
	} else if len(items2) <= 128 {
		result2 = knapsack_128(items2, maxSum2, logger)
	} else {
		result2 = knapsack_n(items2, maxSum2, logger)
	}

	result := make([]int64, 0, len(result2)+1)
	sum := int64(0)
	used := make([]bool, len(items))
	for _, i := range result2 {
		item := items[i]
		result = append(result, item)
		sum += item
		used[i] = true
	}

	// Try to add an element if possible. This can be caused by rounding above.
	left := maxSum - sum
	if left < 0 {
		panic("sum > maxSum")
	}
	for i := len(items) - 1; i >= 0; i-- {
		if used[i] {
			continue
		}
		item := items[i]
		if item <= left {
			result = append(result, item)
			sum += item
			sort.Slice(result, func(i, j int) bool {
				return result[i] < result[j]
			})
			break
		}
	}

	logger("sum is %d / %d\n", sum, maxSum)

	return result
}

func knapsack_32(items []int64, maxSum int64, logger Logger) []int {
	sets := make([]uint32, maxSum+1)

	for i, item := range items {
		logger("\rProcessing item %d/%d.", i+1, len(items))

		mask := uint32(1 << uint(i))

		for j := maxSum; j > item; j-- {
			added := sets[j-item]
			if added == 0 {
				continue
			}
			added |= mask

			existing := sets[j]
			if existing == 0 || bits.OnesCount32(added) > bits.OnesCount32(existing) {
				// Prefer more items to exclude more coins (spend less).
				sets[j] = added
			}
		}
		if sets[item] == 0 {
			sets[item] = mask
		}
	}

	logger("\n")

	var bestSet uint32
	var bestSum uint64
	for j := maxSum; j > 0; j-- {
		if sets[j] != 0 {
			bestSet = sets[j]
			bestSum = uint64(j)
			break
		}
	}

	if bestSet == 0 {
		panic("no sets found")
	}

	result := make([]int, 0, len(items))
	sum := uint64(0)
	for i, item := range items {
		if bestSet&(1<<uint(i)) != 0 {
			result = append(result, i)
			sum += uint64(item)
		}
	}
	if sum != bestSum {
		panic("sum mismatch")
	}

	return result
}

func knapsack_64(items []int64, maxSum int64, logger Logger) []int {
	sets := make([]uint64, maxSum+1)

	for i, item := range items {
		logger("\rProcessing item %d/%d.", i+1, len(items))

		mask := uint64(1 << uint(i))

		for j := maxSum; j > item; j-- {
			added := sets[j-item]
			if added == 0 {
				continue
			}
			added |= mask

			existing := sets[j]
			if existing == 0 || bits.OnesCount64(added) > bits.OnesCount64(existing) {
				// Prefer more items to exclude more coins (spend less).
				sets[j] = added
			}
		}
		if sets[item] == 0 {
			sets[item] = mask
		}
	}

	logger("\n")

	var bestSet uint64
	var bestSum uint64
	for j := maxSum; j > 0; j-- {
		if sets[j] != 0 {
			bestSet = sets[j]
			bestSum = uint64(j)
			break
		}
	}

	if bestSet == 0 {
		panic("no sets found")
	}

	result := make([]int, 0, len(items))
	sum := uint64(0)
	for i, item := range items {
		if bestSet&(1<<uint(i)) != 0 {
			result = append(result, i)
			sum += uint64(item)
		}
	}
	if sum != bestSum {
		panic("sum mismatch")
	}

	return result
}

func knapsack_128(items []int64, maxSum int64, logger Logger) []int {
	sets := make([]uint128.Uint128, maxSum+1)

	one := uint128.From64(1)

	for i, item := range items {
		logger("\rProcessing item %d/%d.", i+1, len(items))

		mask := one.Lsh(uint(i))

		for j := maxSum; j > item; j-- {
			added := sets[j-item]
			if added.IsZero() {
				continue
			}
			added = added.Or(mask)

			existing := sets[j]

			if existing.IsZero() || added.OnesCount() > existing.OnesCount() {
				// Prefer more items to exclude more coins (spend less).
				sets[j] = added
			}
		}
		if sets[item].IsZero() {
			sets[item] = mask
		}
	}

	logger("\n")

	var bestSet uint128.Uint128
	var bestSum uint64
	for j := maxSum; j > 0; j-- {
		if !sets[j].IsZero() {
			bestSet = sets[j]
			bestSum = uint64(j)
			break
		}
	}

	if bestSet.IsZero() {
		panic("no sets found")
	}

	result := make([]int, 0, len(items))
	sum := uint64(0)
	for i, item := range items {
		if !bestSet.And(one.Lsh(uint(i))).IsZero() {
			result = append(result, i)
			sum += uint64(item)
		}
	}
	if sum != bestSum {
		panic("sum mismatch")
	}

	return result
}

func knapsack_n(items []int64, maxSum int64, logger Logger) []int {
	// Little-endian is used.
	intSize := uint((len(items) + 31) / 32)

	isZero := func(slice []uint32) bool {
		for _, b := range slice {
			if b != 0 {
				return false
			}
		}
		return true
	}

	oneCount := func(slice []uint32) (count uint) {
		for _, b := range slice {
			count += uint(bits.OnesCount32(b))
		}
		return
	}

	hasItem := func(slice []uint32, item int) bool {
		return slice[item/32]&(1<<(uint(item)%32)) != 0
	}

	sets := make([]uint32, uint(maxSum+1)*intSize)

	buf := make([]uint32, intSize)

	for i, item := range items {
		logger("\rProcessing item %d/%d.", i+1, len(items))

		maskIndex := i / 32
		maskByte := uint32(1 << (uint(i) % 32))

		for j := maxSum; j > item; j-- {
			addedIndex := uint(j - item)

			added := sets[addedIndex*intSize : (addedIndex+1)*intSize]
			if isZero(added) {
				continue
			}

			copy(buf, added)
			buf[maskIndex] |= maskByte

			existing := sets[uint(j)*intSize : uint(j+1)*intSize]

			if isZero(existing) || oneCount(buf) > oneCount(existing) {
				// Prefer more items to exclude more coins (spend less).
				copy(existing, buf)
			}
		}
		itemSlice := sets[uint(item)*intSize : uint(item+1)*intSize]
		if isZero(itemSlice) {
			itemSlice[maskIndex] = maskByte
		}
	}

	logger("\n")

	bestSet := make([]uint32, intSize)
	var bestSum uint64
	for j := maxSum; j > 0; j-- {
		jSlice := sets[uint(j)*intSize : uint(j+1)*intSize]
		if !isZero(jSlice) {
			bestSet = jSlice
			bestSum = uint64(j)
			break
		}
	}

	if isZero(bestSet) {
		panic("no sets found")
	}

	result := make([]int, 0, len(items))
	sum := uint64(0)
	for i, item := range items {
		if hasItem(bestSet, i) {
			result = append(result, i)
			sum += uint64(item)
		}
	}
	if sum != bestSum {
		panic("sum mismatch")
	}

	return result
}
